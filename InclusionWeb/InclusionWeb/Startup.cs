﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InclusionWeb.Startup))]
namespace InclusionWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
