﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace InclusionWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Default-en",
                url: "{lang}/{controller}/{action}/{id}",
                defaults: new { lang = "en", controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default-ar",
                url: "{lang}/{controller}/{action}/{id}",
                defaults: new { lang = "ar", controller = "Home", action = "IndexAr", id = UrlParameter.Optional }
            );
        }
    }
}
