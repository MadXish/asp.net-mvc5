﻿using InclusionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InclusionWeb.Controllers
{
    public class HomeController : Controller
    {
        [Route("")]
        [Route("en/Home/Index")]
        public ActionResult Index()
        {
            return View();
        }

        [Route("ar/Home/Index")]
        public ActionResult IndexAr()
        {
            return View();
        }

        [Route("en/Home/About")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [Route("en/Home/Contact")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            UserViewModel model;
            model = new UserViewModel
            {
                Firstname = "Ishara",
                Lastname = "Madawa"
            };

            return View(model);
        }


        public ActionResult ModalPopUp()
        {
            return PartialView("ModalPopUp");
        }

        public ActionResult RatingsPopupWidget()
        {
            return PartialView("_ratingsPopupWidget");
        }

        public ActionResult CommonWidget1(UserViewModel model)
        {
            model = new UserViewModel
            {
                Firstname = "Ishara",
                Lastname = "Kumara"
            };

            return PartialView("_commonWidget1", model);
        }

        public ActionResult CommonWidget2(UserViewModel model)
        {
            model = new UserViewModel
            {
                Firstname = "Kumara",
                Lastname = "Madawa"
            };

            return PartialView("_commonWidget2", model);
        }
    }
}