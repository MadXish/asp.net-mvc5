﻿$(document).ready(function () {
    $("#btnShowModal3").click(function (e) {
        e.preventDefault();
        //var id = $(this).data('SelectedRate')
        var rate = $('#SelectedRate').val();
        //$.post('/home/vote', { rate: rate }, function (data) {
        $.post('/home/rate?rate=' + rate, function (data) {
            //$('#surveyWidget').html(data);
            $("#Modal2").modal('hide');
            $("#Modal3").modal('show');
        }).fail(function () {
            $("#ModalError").modal('show');
        })
    })
})


$("#buttonCMN1").click(function () {
    $.ajax({
        url: "CommonWidget1",
        type: "get",
        data: $("form").serialize(),
        success: function (result) {
            $("#_commonWidget1").html(result);
        }
    });
})

$("#buttonCMN2").click(function () {
    $.ajax({
        url: "CommonWidget2",
        type: "get",
        data: $("form").serialize(),
        success: function (result) {
            $("#_commonWidget2").html(result);
        }
    });
})