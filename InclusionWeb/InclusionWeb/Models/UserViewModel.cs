﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InclusionWeb.Models
{
    public class UserViewModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}